<!DOCTYPE html>
<html lang="th" class="isDesktop">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Database</title>
</head>
<style>
  @import url('https://fonts.googleapis.com/css2?family=Prompt&display=swap');
  
  body {
    font-family: 'Bai Jamjuree', sans-serif;
    color: rgb(99, 40, 107);
  }

  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
  }

  li {
    float: left;
  }

  li a {
    display: block;
    text-align: center;
    padding: 14px 16px;
  }

  li a:hover:not(.active) {
    background-color: rgb(99, 40, 107);
    color: rgb(255, 255, 255);
  }
  .active {
    background-color: rgb(99, 40, 107);
    color: rgb(255, 255, 255);
  }
  nav {
    font-family: 'Prompt', sans-serif;
    background-color: #e3cfe7;
  }

  span {
    display: block;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    color: rgb(99, 40, 107);
  }

  div {
    color: rgb(99, 40, 107);
  }

  img {
    border-radius: 50%;
  }
  .header {
  background-color: #e3cfe7;
  padding: 5px;
}
.text {
  font-family: 'Bai Jamjuree', sans-serif;
  color: rgb(69, 22, 77);
  margin-left: 10%;
  margin-right: 10%;
}
.button {
  font-family: 'Prompt', sans-serif;
  color: rgb(255, 255, 255);
  background-color: rgb(99, 40, 107);
}
.bg {
  background-color: #faf6fc;
}
h1 {
  font-family: 'Prompt', sans-serif;
  color: rgb(69, 22, 77);
}
h5 {
  font-family: 'Prompt', sans-serif;
  color: rgb(69, 22, 77);
  margin-left: 10%;
  margin-right: 10%;
}
</style>
<body>
<div class="header"></div>
  <nav class="navbar navbar-expand-sm">
    <div class="container-fluid">
      <span class="navbar-brand">
        <img src="img/avatarbew.jpg" alt="Avatar Logo" style="width:40px;" class="rounded-pill"> 
      </span>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.html">หน้าหลัก</a>
              </li>
          <li class="nav-item">
            <a class="nav-link" href="interested.html">สิ่งที่สนใจ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about_su.html">รอบรั้วศิลปากร</a>
          </li>
          <li class="nav-item">
            <span class="navbar-text">Connect database</span>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<?php
   $servername = "db";
   $username = "devops";
   $password = "devops101";

   $dbhandle = mysqli_connect($servername, $username, $password);
   $selected = mysqli_select_db($dbhandle, "titanic");
   
   echo "Connected database server<br>";
   echo "Selected database";
?>
<div class="w3-container bg w3-center">
    <a href="index.html" class="btn w3-button w3-margin-bottom button"><i class="fa fa-home"></i>  หน้าหลัก</a>
    <a href="interested.html" class="btn w3-button w3-margin-bottom button"><i class="fa fa-heart"></i>  สิ่งที่สนใจ</a>
    <a href="about_su.html" class="btn w3-button w3-margin-bottom button"><i class="fa fa-map-marker"></i>  รอบรั้วศิลปากร</a>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
    crossorigin="anonymous"></script>
</body>
</html>
